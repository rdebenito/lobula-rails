class WrapPersonPictureIntoPaperclip < ActiveRecord::Migration
  def change
    change_table :people do |t|
      t.remove :picture
      t.attachment :bitmap
    end
  end
end
