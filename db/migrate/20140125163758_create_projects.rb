class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.text :summary
      t.text :description
      t.string :client
      t.string :credit
      t.string :role
      t.date :date

      t.timestamps
    end
  end
end
