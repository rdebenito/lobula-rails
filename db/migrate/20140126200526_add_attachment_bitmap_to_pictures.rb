class AddAttachmentBitmapToPictures < ActiveRecord::Migration
  def self.up
    change_table :pictures do |t|
      t.attachment :bitmap
    end
  end

  def self.down
    drop_attached_file :pictures, :bitmap
  end
end
