class AddAttachmentBitmapToProjects < ActiveRecord::Migration
  def self.up
    change_table :projects do |t|
      t.attachment :bitmap
    end
  end

  def self.down
    drop_attached_file :projects, :bitmap
  end
end
