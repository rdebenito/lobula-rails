class AddAwardsToProject < ActiveRecord::Migration
  def change
    add_column :projects, :awards, :text
  end
end
