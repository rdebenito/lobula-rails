class RenameProjectUrlToWebsite < ActiveRecord::Migration
  def change
    rename_column :projects, :url, :website_url
  end
end
