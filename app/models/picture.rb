class Picture < ActiveRecord::Base
  belongs_to :project
  has_attached_file :bitmap, :styles => {
    :large => ["1280", :jpg],
    :medium => ["640", :jpg],
    :small => ["320", :jpg],
    :thumb => ["160x160#", :jpg]
    }
end
