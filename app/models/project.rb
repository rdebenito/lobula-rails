class Project < ActiveRecord::Base

  validates_presence_of :title

  before_create :create_slug

  has_attached_file :bitmap, :styles => {
    :small => ["350", :jpg],
    :square => ["350x350#", :jpg]
  }

  def to_param
    slug
  end

  def create_slug
    self.slug = self.title.parameterize
  end
end
