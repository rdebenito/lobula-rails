class MainController < ApplicationController
  def index
    main_page()
  end

  def project_page
    if Project.exists?(slug: params[:id])
      @project = Project.find_by :slug => params[:id]
      @pictures = Picture.where :project_id => @project.id
      logger.info @pictures
      main_page()
      return;
    else
      render_not_found()
    end
  end

  private
    def main_page
      @person = Person.first()
      @projects = Project.all
      render "main/index"
    end
end
